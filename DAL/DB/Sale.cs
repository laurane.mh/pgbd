﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MyPopUpStore_LM.DAL.DB
{
    public partial class Sale
    {
        public Sale()
        {
            ProductLists = new HashSet<ProductList>();
        }

        public int IdSale { get; set; }
        public DateTime DateTime { get; set; }
        public string PaymentMeans { get; set; }

        public virtual ICollection<ProductList> ProductLists { get; set; }
    }
}
