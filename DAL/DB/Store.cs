﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MyPopUpStore_LM.DAL.DB
{
    public partial class Store
    {
        public Store()
        {
            Prices = new HashSet<Price>();
            Products = new HashSet<Product>();
        }

        public int IdStore { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Price> Prices { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
