﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace MyPopUpStore_LM.DAL.DB
{
    public partial class MyPopUpStoreLauraneContext : DbContext
    {
        public MyPopUpStoreLauraneContext()
        {
        }

        public MyPopUpStoreLauraneContext(DbContextOptions<MyPopUpStoreLauraneContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Price> Prices { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductList> ProductLists { get; set; }
        public virtual DbSet<Sale> Sales { get; set; }
        public virtual DbSet<Store> Stores { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=LAPTOP-N00SQK3E\\sqlexpress01;Database=MyPopUpStoreLaurane;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "French_CI_AS");

            modelBuilder.Entity<Price>(entity =>
            {
                entity.HasKey(e => e.IdPrice)
                    .HasName("PK__price__D8A23E83F35B3E7C");

                entity.ToTable("price");

                entity.Property(e => e.IdPrice).HasColumnName("id_price");

                entity.Property(e => e.Amount).HasColumnName("amount");

                entity.Property(e => e.Color)
                    .IsRequired()
                    .HasMaxLength(80)
                    .HasColumnName("color");

                entity.Property(e => e.IdStore).HasColumnName("id_store");

                entity.HasOne(d => d.IdStoreNavigation)
                    .WithMany(p => p.Prices)
                    .HasForeignKey(d => d.IdStore)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__price__id_store__02C769E9");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.HasKey(e => e.IdProduct)
                    .HasName("PK__product__BA39E84FDB18D6EE");

                entity.ToTable("product");

                entity.HasIndex(e => e.ProductCode, "UQ__product__AE1A8CC4D9F528EF")
                    .IsUnique();

                entity.Property(e => e.IdProduct).HasColumnName("id_product");

                entity.Property(e => e.IdPrice).HasColumnName("id_price");

                entity.Property(e => e.IdStore).HasColumnName("id_store");

                entity.Property(e => e.ProductCode)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("product_code")
                    .IsFixedLength(true);

                entity.Property(e => e.Stock).HasColumnName("stock");

                entity.Property(e => e.Wording)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("wording");

                entity.HasOne(d => d.IdPriceNavigation)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.IdPrice)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__product__id_pric__078C1F06");

                entity.HasOne(d => d.IdStoreNavigation)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.IdStore)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__product__id_stor__0697FACD");
            });

            modelBuilder.Entity<ProductList>(entity =>
            {
                entity.HasKey(e => new { e.IdSale, e.IdProduct })
                    .HasName("PK__product___3A289FD3F9FC504A");

                entity.ToTable("product_list");

                entity.Property(e => e.IdSale).HasColumnName("id_sale");

                entity.Property(e => e.IdProduct).HasColumnName("id_product");

                entity.Property(e => e.Quantity).HasColumnName("quantity");

                entity.HasOne(d => d.IdProductNavigation)
                    .WithMany(p => p.ProductLists)
                    .HasForeignKey(d => d.IdProduct)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__product_l__id_pr__0D44F85C");

                entity.HasOne(d => d.IdSaleNavigation)
                    .WithMany(p => p.ProductLists)
                    .HasForeignKey(d => d.IdSale)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__product_l__id_sa__0C50D423");
            });

            modelBuilder.Entity<Sale>(entity =>
            {
                entity.HasKey(e => e.IdSale)
                    .HasName("PK__sale__D18B015737EA89F7");

                entity.ToTable("sale");

                entity.Property(e => e.IdSale).HasColumnName("id_sale");

                entity.Property(e => e.DateTime)
                    .HasColumnType("datetime")
                    .HasColumnName("date_time");

                entity.Property(e => e.PaymentMeans)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("payment_means");
            });

            modelBuilder.Entity<Store>(entity =>
            {
                entity.HasKey(e => e.IdStore)
                    .HasName("PK__store__3A39C339EFDED1B7");

                entity.ToTable("store");

                entity.Property(e => e.IdStore).HasColumnName("id_store");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(150)
                    .HasColumnName("name");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
