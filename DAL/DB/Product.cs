﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MyPopUpStore_LM.DAL.DB
{
    public partial class Product
    {
        public Product()
        {
            ProductLists = new HashSet<ProductList>();
        }

        public int IdProduct { get; set; }
        public string ProductCode { get; set; }
        public string Wording { get; set; }
        public int Stock { get; set; }
        public int IdStore { get; set; }
        public int IdPrice { get; set; }

        public virtual Price IdPriceNavigation { get; set; }
        public virtual Store IdStoreNavigation { get; set; }
        public virtual ICollection<ProductList> ProductLists { get; set; }
    }
}
