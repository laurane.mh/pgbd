﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MyPopUpStore_LM.DAL.DB
{
    public partial class Price
    {
        public Price()
        {
            Products = new HashSet<Product>();
        }

        public int IdPrice { get; set; }
        public string Color { get; set; }
        public int Amount { get; set; }
        public int IdStore { get; set; }

        public virtual Store IdStoreNavigation { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
