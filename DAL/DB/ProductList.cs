﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MyPopUpStore_LM.DAL.DB
{
    public partial class ProductList
    {
        public int IdSale { get; set; }
        public int IdProduct { get; set; }
        public int Quantity { get; set; }

        public virtual Product IdProductNavigation { get; set; }
        public virtual Sale IdSaleNavigation { get; set; }
    }
}
