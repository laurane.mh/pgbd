﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MyPopUpStore_LM.UI.Views;
using MyPopUpStore_LM.BU;
using MyPopUpStore_LM.UI.ViewModels;
using System.Collections.ObjectModel;
using MyPopUpStore_LM.DAL.DB;

namespace MyPopUpStore_LM
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        StoreListViewModel storeListViewModel;

        public MainWindow()
        {
            InitializeComponent();
            PopulateAndBind();
        }
        private void PopulateAndBind()
        {
            storeListViewModel = new StoreListViewModel()
            {
                Stores = new ObservableCollection<Store>(StoreService.GetStores())
            };
            this.DataContext = storeListViewModel;
        }

        private void ChooseCreateStoreButton_Click(object sender, RoutedEventArgs e)
        {
            StoreView storeViewWindow = new StoreView();
            storeViewWindow.Show();
            this.Close();
        }

        private void SelectStoreButton_Click(object sender, RoutedEventArgs e)
        {
            storeListViewModel.Store = (Store)SelectStoreComboBox.SelectedItem;
            
            HomeView homeViewWindow = new HomeView(storeListViewModel.Store);
            homeViewWindow.Show();
            this.Close();
        }

        private void EditStoreButton_Click(object sender, RoutedEventArgs e)
        {
            Store store = (Store)SelectStoreComboBox.SelectedItem;
            StoreView storeViewWindow = new StoreView(store);
            storeViewWindow.Show();
            this.Close();
        }
    }
}
