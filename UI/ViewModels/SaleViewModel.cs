﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyPopUpStore_LM.DAL.DB;

namespace MyPopUpStore_LM.UI.ViewModels
{
    class SaleViewModel
    {
        public Store Store { get; set; }

        public Sale Sale { get; set; }

        public List<Product> product { get; set; }

        public ProductList productList { get; set; }
        

        private ObservableCollection<ProductListTotalViewModel> productLists;

        public ObservableCollection<ProductListTotalViewModel> ProductLists
        {
            get { return productLists; }
            set { productLists = value; }
        }

        private ObservableCollection<Product> products;

        public ObservableCollection<Product> Products
        {
            get { return products; }
            set { products = value; }
        }

        public int Total { get; set; }

        public virtual ICollection<ProductList> ProductsLists { get; set; }

    }
}
