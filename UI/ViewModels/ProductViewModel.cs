﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyPopUpStore_LM.DAL.DB;

namespace MyPopUpStore_LM.UI.ViewModels
{
    class ProductViewModel
    {
        public Store Store { get; set; }
        public string ProductCode { get; set; }
        public string Wording { get; set; }

        public short Stock { get; set; }
        public Price Price { get; set; }
        public int Amount { get; set; }


        public Product Product { get; set; }
        public List<Price> PriceCategories { get; set; }
    }
}
