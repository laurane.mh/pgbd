﻿using MyPopUpStore_LM.DAL.DB;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPopUpStore_LM.UI.ViewModels
{
    class StoreListViewModel
    {
        public Store Store { get; set; }

        private ObservableCollection<Store> stores = new ObservableCollection<Store>();

        public ObservableCollection<Store> Stores
        {
            get { return stores; }
            set { stores = value; }
        }

    }
}
