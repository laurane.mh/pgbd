﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using MyPopUpStore_LM.DAL.DB;

namespace MyPopUpStore_LM.UI.ViewModels
{
    class SaleListViewModel
    {
        public Store Store { get; set; }

        private ObservableCollection<Sale> sales;

        public ObservableCollection<Sale> Sales
        {
            get { return sales; }
            set { sales = value; }
        }

    }
}
