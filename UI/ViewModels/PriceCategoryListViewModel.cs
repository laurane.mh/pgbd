﻿using MyPopUpStore_LM.DAL.DB;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPopUpStore_LM.UI.ViewModels
{
    class PriceCategoryListViewModel
    {
        public Store Store { get; set; }

        private ObservableCollection<Price> priceCategories;

        public ObservableCollection<Price> PriceCategories
        {
            get { return priceCategories; }
            set { priceCategories = value; }
        } /*Permet de maj le PriceCategories */

    }
}
