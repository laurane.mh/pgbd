﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using MyPopUpStore_LM.DAL.DB;

namespace MyPopUpStore_LM.UI.ViewModels
{
    class ProductListViewModel
    {
        public Store Store { get; set; }

        public Price Price { get; set; }

        private ObservableCollection<Product> products;

        public ObservableCollection<Product> Products
        {
            get { return products; }
            set { products = value; }
        }
    }

}
