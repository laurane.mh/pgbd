﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPopUpStore_LM.UI.ViewModels
{
    class ProductListTotalViewModel
    {
        public string ProductCode { get; set; }
        public int Quantity { get; set; }

        public int UnitPrice { get; set; }

        public int Total { get; set; }
    }
}
