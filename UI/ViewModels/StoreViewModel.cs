﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyPopUpStore_LM.DAL.DB;

namespace MyPopUpStore_LM.UI.ViewModels
{
    class StoreViewModel
    {
        public Store Store { get; set; }
        public String Name { get; set; }
        public int IdStore {get; set;}

        private ObservableCollection<Store> stores = new ObservableCollection<Store>();

        public ObservableCollection<Store> Stores
        {
            get { return stores; }
            set { stores = value; }
        }
        
    }
    
}
