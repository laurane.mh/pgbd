﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyPopUpStore_LM.DAL.DB;

namespace MyPopUpStore_LM.UI.ViewModels
{
    class PriceCategoryViewModel
    {
        public Store Store { get; set; }
        public short Amount{ get; set; }
        public String Color { get; set; }

        public Price Price { get; set; }

        public ObservableCollection<Price> Prices { get; set; }

        public PriceCategoryViewModel()
        {
            Prices = new ObservableCollection<Price>();
        }
    }
}
