﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MyPopUpStore_LM.UI.Views;
using MyPopUpStore_LM.UI.ViewModels;
using System.Collections.ObjectModel;
using MyPopUpStore_LM.DAL.DB;
using MyPopUpStore_LM.BU;

namespace MyPopUpStore_LM.UI.Views
{
    /// <summary>
    /// Logique d'interaction pour SaleListView.xaml
    /// </summary>
    public partial class SaleListView : Window
    {
        SaleListViewModel saleListViewModel;
        public SaleListView(Store store)
        {
            InitializeComponent();
            PopulateAndBind(store);
        }

        private void PopulateAndBind(Store store)
        {
            saleListViewModel = new SaleListViewModel()
            {
                Sales = new ObservableCollection<Sale>(SaleService.GetSales()),
                Store = store
            };
            this.DataContext = saleListViewModel;
        }

        private void ReturnBackButton_Click(object sender, RoutedEventArgs e)
        {
            HomeView homeViewWindow = new HomeView(saleListViewModel.Store);
            homeViewWindow.Show();
            this.Close();
        }

        private void ChooseCreateSaleButton_Click(object sender, RoutedEventArgs e)
        {
            SaleView saleViewWindow = new SaleView(saleListViewModel.Store);
            saleViewWindow.Show();
            this.Close();
        }

        //private void ChooseEditSaleButton_Click(object sender, RoutedEventArgs e)
        //{
        //    Sale sale = (Sale)SaleListDataGrid.SelectedItem;
        //    SaleView saleViewWindow = new SaleView(saleListViewModel.Store, sale);
        //    saleViewWindow.Show();
        //    this.Close();
        //}

        private void ChooseDeleteSaleButton_Click(object sender, RoutedEventArgs e)
        {
            var saleId = ((Sale)SaleListDataGrid.SelectedItem).IdSale;
            SaleService.DeleteSale(saleId);
            MessageBox.Show("Sale deleted");
            SaleListView saleListViewWindow = new SaleListView(saleListViewModel.Store);
            saleListViewWindow.Show();
            this.Close();
        }

    }
}
 