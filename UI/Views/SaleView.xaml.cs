﻿using MyPopUpStore_LM.BU;
using MyPopUpStore_LM.DAL.DB;
using MyPopUpStore_LM.UI.ViewModels;
using System;
using System.Collections.ObjectModel;
using System.Windows;

namespace MyPopUpStore_LM.UI.Views
{
    /// <summary>
    /// Logique d'interaction pour SaleView.xaml
    /// </summary>
    public partial class SaleView : Window
    {
        SaleViewModel saleViewModel;
        public SaleView(Store store)
        {
            
            InitializeComponent();
            PopulateAndBind(store); 
        }
        
        public SaleView(Store store, Sale sale)
        {
            
            InitializeComponent();
            PopulateAndBind(store, sale); 
        }

        private void PopulateAndBind(Store store)
        {
            saleViewModel = new SaleViewModel()
            {
                ProductLists = new ObservableCollection<ProductListTotalViewModel>(),
                product =  ProductService.GetProducts(),
                Products = new ObservableCollection<Product>(ProductService.GetProductsOfStore(store.IdStore)),
                Store = store,
                Sale = new Sale()
            };
            this.DataContext = saleViewModel;
        }
        
        private void PopulateAndBind(Store store, Sale sale)
        {
            saleViewModel = new SaleViewModel()
            {
                ProductLists = new ObservableCollection<ProductListTotalViewModel>(),
                product =  ProductService.GetProducts(),
                Products = new ObservableCollection<Product>(ProductService.GetProductsOfStore(store.IdStore)),
                Store = store,
                Sale = sale
            };
            this.DataContext = saleViewModel;
        }

        private void ReturnBackButton_Click(object sender, RoutedEventArgs e)
        {
            SaleListView saleListViewWindow = new SaleListView(saleViewModel.Store);
            saleListViewWindow.Show();
            this.Close();
        }

        private void AddSaleButton_Click(object sender, RoutedEventArgs e)
        {
            if (saleViewModel.Sale.IdSale != 0)
            {
                SaleService.EditSale(saleViewModel.Sale);
                MessageBox.Show("Sale edited");
            }

            else
            {
                int IdSale = SaleService.AddSales(new Sale() 
                {
                    PaymentMeans = PaymentComboBox.Text,
                    DateTime = DateTime.Now      
                });

                foreach (ProductListTotalViewModel productListViewModel in saleViewModel.ProductLists) 
                {
                    ProductListService.AddProductList(new ProductList()
                    {
                        IdSale = IdSale,
                        IdProduct = ProductService.GetIdProductByCode(productListViewModel.ProductCode),
                        Quantity = productListViewModel.Quantity                    
                    }); 
                }
                MessageBox.Show("Sale created");
            }
            PopulateAndBind(saleViewModel.Store);
            SaleListView saleListViewWindow = new SaleListView(saleViewModel.Store);
            saleListViewWindow.Show();
            this.Close();
        }

        private void AddProductCartButton_Click(object sender, RoutedEventArgs e)
        {
            Product product = (Product)SelectProductComboBox.SelectedItem;
            ProductListTotalViewModel productList = new ProductListTotalViewModel();

            if(Int32.TryParse(QuantityTextBox.Text, out int quantity))
            {
                productList.ProductCode = product.ProductCode; 
                productList.Quantity = quantity;
                productList.UnitPrice = product.IdPriceNavigation.Amount;
                productList.Total = quantity * product.IdPriceNavigation.Amount;

                saleViewModel.ProductLists.Add(productList);
            }
            else
            {
                MessageBox.Show("Please put an integer.");
            }

        }
    }
}
