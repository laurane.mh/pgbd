﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MyPopUpStore_LM.UI.ViewModels;
using MyPopUpStore_LM.UI.Views;
using MyPopUpStore_LM.DAL.DB;
using MyPopUpStore_LM.BU;

namespace MyPopUpStore_LM.UI.Views
{
    /// <summary>
    /// Logique d'interaction pour ProductView.xaml
    /// </summary>
    public partial class ProductView : Window
    {
        ProductViewModel productViewModel;

        public object IdPriceNavigation { get; private set; }

        public ProductView(Store store)
        {
            InitializeComponent();
            PopulateAndBind(store);
        }

        public ProductView(Store store, Product product)
        {
            InitializeComponent();
            PopulateAndBind(store, product);
        }

        private void PopulateAndBind(Store store)
        {
            productViewModel = new ProductViewModel()
            {
                PriceCategories = PriceCategoryService.GetPricesCategoriesOfStore(store.IdStore),
                Store = store,
                Product = new Product()
                
            };
            this.DataContext = productViewModel;
        }

        private void PopulateAndBind(Store store, Product product)
        {
            productViewModel = new ProductViewModel()
            {
                PriceCategories = PriceCategoryService.GetPricesCategoriesOfStore(store.IdStore),
                Store = store,
                Product = product, 
                Price = product.IdPriceNavigation
            };
            this.DataContext = productViewModel;
        }

        private void ReturnBackButton_Click(object sender, RoutedEventArgs e)
        {
            ProductListView productListViewWindow = new ProductListView(productViewModel.Store);
            productListViewWindow.Show();
            this.Close();
        }

        private void CreateProductButton_Click(object sender, RoutedEventArgs e)
        {

            if (productViewModel.Product.IdProduct != 0)
            {
                ProductService.EditProduct(productViewModel.Product);
                MessageBox.Show("Product edited");
            }

            else
            {

                ProductService.AddProduct(new Product()
                {
                    ProductCode = productViewModel.Product.ProductCode,
                    Wording = productViewModel.Product.Wording,
                    Stock = productViewModel.Product.Stock,
                    IdPrice = productViewModel.Product.IdPrice,
                    IdStore = productViewModel.Store.IdStore
                });
                MessageBox.Show("Product created");
            }

            ProductListView productListViewWindow = new ProductListView(productViewModel.Store);
            productListViewWindow.Show();
            this.Close();
        }

        
    }


}
