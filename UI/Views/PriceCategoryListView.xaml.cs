﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MyPopUpStore_LM.UI.Views;
using MyPopUpStore_LM.UI.ViewModels;
using System.Collections.ObjectModel;
using MyPopUpStore_LM.DAL.DB;
using MyPopUpStore_LM.BU;

namespace MyPopUpStore_LM.UI.Views
{
    /// <summary>
    /// Logique d'interaction pour PriceCategoryListView.xaml
    /// </summary>
    public partial class PriceCategoryListView : Window
    {
        PriceCategoryListViewModel priceCategoryListViewModel;
        

        public PriceCategoryListView(Store store)
        {
            InitializeComponent();
            PopulateAndBind(store);
        }

        private void PopulateAndBind(Store store)
        {
            priceCategoryListViewModel = new PriceCategoryListViewModel()
            {
                PriceCategories = new ObservableCollection<Price>(PriceCategoryService.GetPricesCategoriesOfStore(store.IdStore)),
                Store = store
            };
            this.DataContext = priceCategoryListViewModel;
        }

        private void ReturnBackButton_Click(object sender, RoutedEventArgs e)
        {
            HomeView homeViewWindow = new HomeView(priceCategoryListViewModel.Store);
            homeViewWindow.Show();
            this.Close();
        }

        private void ChooseCreatePriceCategoryButton_Click(object sender, RoutedEventArgs e)
        {
            PriceCategoryView priceCategoryViewWindow = new PriceCategoryView(priceCategoryListViewModel.Store);
            priceCategoryViewWindow.Show();
            this.Close();
        }
        
        private void ChooseEditPriceCategoryButton_Click(object sender, RoutedEventArgs e)
        {
            Price price = (Price)PriceCategoryListDataGrid.SelectedItem;
            PriceCategoryView priceCategoryViewWindow = new PriceCategoryView(priceCategoryListViewModel.Store, price);
            priceCategoryViewWindow.Show();
            this.Close();
        }

        private void DeletePriceCategoryButton_Click(object sender, RoutedEventArgs e)
        {
            var priceId = ((Price)PriceCategoryListDataGrid.SelectedItem).IdPrice;

            if (PriceCategoryService.ProductHasPriceCategory(priceId))
            {
                MessageBox.Show("Product are associated to this price category, you have to change the price category for the product(s) concerned.");
            }

            else
            {
                PriceCategoryService.DeletePriceCategory(priceId);
                MessageBox.Show("Price deleted");
                PriceCategoryView priceCategoryViewWindow = new PriceCategoryView(priceCategoryListViewModel.Store, null);
                priceCategoryViewWindow.Show();
                this.Close();
            }
        }

        
    }
}
