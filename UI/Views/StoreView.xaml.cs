﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MyPopUpStore_LM.UI.ViewModels;
using MyPopUpStore_LM.UI.Views;
using MyPopUpStore_LM.DAL.DB;
using MyPopUpStore_LM.BU;
using System.Collections.ObjectModel;


namespace MyPopUpStore_LM.UI.Views
{
    /// <summary>
    /// Logique d'interaction pour StoreView.xaml
    /// </summary>
    public partial class StoreView : Window
    {
        StoreViewModel storeViewModel;

        public StoreView()
        {
            InitializeComponent();
            PopulateAndBind();
        }
        public StoreView(Store store)
        {
            InitializeComponent();
            PopulateAndBind(store);
        }

        private void PopulateAndBind()
        {
            storeViewModel = new StoreViewModel()
            {
                Stores = new ObservableCollection<Store>(StoreService.GetStores()),
                Store = new Store()
            };
            this.DataContext = storeViewModel;
        }
        private void PopulateAndBind(Store store)
        {
            storeViewModel = new StoreViewModel()
            {
                Stores = new ObservableCollection<Store>(StoreService.GetStores()),
                Store = store, 

            };
            this.DataContext = storeViewModel;
        }
        
        
        private void CreateStoreButton_Click(object sender, RoutedEventArgs e)
        {

            if (storeViewModel.Store.IdStore != 0)
            {
                StoreService.EditStore(storeViewModel.Store);
            }
            else
            {
                StoreService.AddStore(new Store()
                {
                    Name = storeViewModel.Store.Name
                });
            }
            MainWindow mainViewWindow = new MainWindow();
            mainViewWindow.Show();
            this.Close();       
        }

        private void ChangeStoreButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainViewWindow = new MainWindow();
            mainViewWindow.Show();
            this.Close();
        }

        private void ReturnBackButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainViewWindow = new MainWindow();
            mainViewWindow.Show();
            this.Close();
        }
        


        private void DeleteStoreButton_Click(object sender, RoutedEventArgs e)
        {
            
            var storeId = ((Store)StoreListDataGrid.SelectedItem).IdStore;

            if (StoreService.StoreHasProduct(storeId)) 
            {
                MessageBox.Show("There's Product on this Store, you can't delete the store.");
            }
            else
            {
                StoreService.DeleteStore(storeId); 
                MainWindow mainViewWindow = new MainWindow();
                mainViewWindow.Show();
                this.Close();
            }
        }    
    }
}
