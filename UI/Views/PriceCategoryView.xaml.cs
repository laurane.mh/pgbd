﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MyPopUpStore_LM.BU;
using MyPopUpStore_LM.DAL.DB;
using MyPopUpStore_LM.UI.ViewModels;
using MyPopUpStore_LM.UI.Views;

namespace MyPopUpStore_LM.UI.Views
{
    /// <summary>
    /// Logique d'interaction pour PriceCategoryView.xaml
    /// </summary>
    public partial class PriceCategoryView : Window
    {
        PriceCategoryViewModel priceCategoryViewModel;

        public PriceCategoryView(Store store)
        {
            InitializeComponent();
            PopulateAndBind(store);
        }

        public PriceCategoryView(Store store, Price price) 
        {
            InitializeComponent();
            PopulateAndBind(store, price); 
        }

        private void PopulateAndBind(Store store)
        {
            priceCategoryViewModel = new PriceCategoryViewModel() 
            {
                Store = store,
                Price = new Price()
            };

            this.DataContext = priceCategoryViewModel;
        }

        private void PopulateAndBind(Store store, Price price)
        {
            priceCategoryViewModel = new PriceCategoryViewModel() 
            {
                Store = store,
                Price = price
            };

            this.DataContext = priceCategoryViewModel;
        }


        private void ReturnBackButton_Click(object sender, RoutedEventArgs e)
        {
            PriceCategoryListView priceCategoryListViewWindow = new PriceCategoryListView(priceCategoryViewModel.Store);
            priceCategoryListViewWindow.Show();
            this.Close();
        }

        private void CreatePriceCategoryButton_Click(object sender, RoutedEventArgs e)
        {

            if (priceCategoryViewModel.Price.IdPrice != 0)
            {
                if (priceCategoryViewModel.Price.Amount != 0 || priceCategoryViewModel.Price.Color != null)
                {
                    PriceCategoryService.EditPriceCategory(priceCategoryViewModel.Price);
                    MessageBox.Show("Price edited");
                }
                else
                {
                    MessageBox.Show("An TextBox cannot be empty.");
                }

            }
            else
            {

                PriceCategoryService.AddPriceCategory(new Price()
                {
                    Amount = priceCategoryViewModel.Price.Amount,
                    Color = priceCategoryViewModel.Price.Color,
                    IdStore = priceCategoryViewModel.Store.IdStore
                });
                MessageBox.Show("Price created");
            }

            PriceCategoryListView priceCategoryListViewWindow = new PriceCategoryListView(priceCategoryViewModel.Store);
            priceCategoryListViewWindow.Show();
            this.Close();
        }

        private void DeletePriceCategoryButton_Click(object sender, RoutedEventArgs e)
        {
            /*DELETE*/
        }
    }
}
