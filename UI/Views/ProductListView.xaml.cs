﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MyPopUpStore_LM.UI.Views;
using MyPopUpStore_LM.UI.ViewModels;
using System.Collections.ObjectModel;
using MyPopUpStore_LM.DAL.DB;
using MyPopUpStore_LM.BU;

namespace MyPopUpStore_LM.UI.Views
{
    /// <summary>
    /// Logique d'interaction pour ProductListView.xaml
    /// </summary>
    public partial class ProductListView : Window
    {
        ProductListViewModel productListViewModel;
        public ProductListView(Store store)
        {
            InitializeComponent();
            PopulateAndBind(store);
        }

        private void PopulateAndBind(Store store)
        {
            productListViewModel = new ProductListViewModel()
            {
                Products = new ObservableCollection<Product>(ProductService.GetProductsOfStore(store.IdStore)),
                Store = store
            };
            this.DataContext = productListViewModel;
        }

        private void ReturnBackButton_Click(object sender, RoutedEventArgs e)
        {
            HomeView homeViewWindow = new HomeView(productListViewModel.Store);
            homeViewWindow.Show();
            this.Close();
        }

        private void ChooseCreateProductButton_Click(object sender, RoutedEventArgs e)
        {
            ProductView productViewWindow = new ProductView(productListViewModel.Store);
            productViewWindow.Show();
            this.Close();
        }

        private void ChooseEditProductButton_Click(object sender, RoutedEventArgs e)
        {
            Product product = (Product)ProductListDataGrid.SelectedItem;
            ProductView productViewWindow = new ProductView(productListViewModel.Store, product);
            productViewWindow.Show();
            this.Close();
        }

        private void ChooseDeleteProductButton_Click(object sender, RoutedEventArgs e)
        {
            var productId = ((Product)ProductListDataGrid.SelectedItem).IdProduct;
            if (ProductService.SaleHasProduct(productId)) 
            {
                MessageBox.Show("This product can't be deleted because he's in a sale. Delete the sale beforehand.");
            }
            else 
            {
                ProductService.DeleteProduct(productId);
                MessageBox.Show("Product deleted");
                ProductListView productListViewWindow = new ProductListView(productListViewModel.Store);
                productListViewWindow.Show();
                this.Close();
            }
        }
    }
}
