﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MyPopUpStore_LM.UI.Views;
using MyPopUpStore_LM.UI.ViewModels;
using MyPopUpStore_LM.DAL.DB;

namespace MyPopUpStore_LM.UI.Views
{
    /// <summary>
    /// Logique d'interaction pour HomeView.xaml
    /// </summary>
    public partial class HomeView : Window
    {
        HomepageViewModel homepageViewModel;

        public HomeView(Store store)
        {
            InitializeComponent();
            PopulateAndBind(store);
        }

        private void PopulateAndBind(Store store)
        {
            homepageViewModel = new HomepageViewModel()
            {
                Store = store
            };          
        }

        private void ChangeStoreButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainViewWindow = new MainWindow();
            mainViewWindow.Show();
            this.Close();
        }

        private void SalesButton_Click(object sender, RoutedEventArgs e)
        {
            SaleListView saleListViewWindow = new SaleListView(homepageViewModel.Store);
            saleListViewWindow.Show();
            this.Close();
        }

        private void ProductButton_Click(object sender, RoutedEventArgs e)
        {
            ProductListView productListViewWindow = new ProductListView(homepageViewModel.Store);
            productListViewWindow.Show();
            this.Close();
        }

        private void PriceCategoryButton_Click(object sender, RoutedEventArgs e)
        {
            PriceCategoryListView priceCategoryListViewWindow = new PriceCategoryListView(homepageViewModel.Store);
            priceCategoryListViewWindow.Show();
            this.Close();
        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            StoreView storeViewWindow = new StoreView(homepageViewModel.Store);
            storeViewWindow.Show();
            this.Close();
        }

        private void ChangeStoreButton_Click_1(object sender, RoutedEventArgs e)
        {
            MainWindow mainViewWindow = new MainWindow();
            mainViewWindow.Show();
            this.Close();
        }


    }
}
