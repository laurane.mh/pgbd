﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MyPopUpStore_LM.DAL.DB;

namespace MyPopUpStore_LM.BU
{
    class StoreService
    {
        //public static Store GetStoreById(int StoreId)
        //{
        //    using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
        //    {
        //        return db.Stores.Find(StoreId);
        //    }
        //}

        public static List<Store> GetStores()
        {
            using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
            {
                return db.Stores.ToList();
            }
        }

        //public static List<Product> GetProductsOfStore(int storeId)
        //{
        //    using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
        //    {
        //        return db.Products.Where(p => p.IdStore == storeId).Include(p => p.IdPriceNavigation).ToList();
        //    }
        //}

        public static void AddStore(Store store)
        {
            using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
            {
                db.Stores.Add(store);
                db.SaveChanges();
            }
        }

        public static void EditStore(Store store)
        {
            using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
            {
                Store storeToUpdate = db.Stores.Find(store.IdStore);
                {
                    if (storeToUpdate != null)
                    {
                        storeToUpdate.Name = store.Name;

                        db.Update(storeToUpdate);
                    }
                }
                db.SaveChanges();
            }
        }

        public static void DeleteStore(int storeId)
        {
            using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
            {
                var storeToRemove = db.Stores.SingleOrDefault(a => a.IdStore == storeId);
                if (storeToRemove != null)
                {
                    db.Stores.Remove(storeToRemove);
                    db.SaveChanges();
                }
            }
        }

        public static bool StoreHasProduct(int storeId)
        {
            using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
            {
                return db.Products.Where(s => s.IdStore == storeId).Count() > 0;
            }
        }


    }
}
    
