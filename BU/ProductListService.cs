﻿using MyPopUpStore_LM.DAL.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPopUpStore_LM.BU
{
    class ProductListService
    {
        public static List<ProductList> GetProductListsOfSale(int saleId)
        {
            using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
            {
                return db.ProductLists.Where(p => p.IdSale == saleId).ToList();
            }
        }

        public static void AddProductList(ProductList productList)
        {
            using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
            {
                db.ProductLists.Add(productList);
                db.SaveChanges();
            }
        }



        public static void DeleteProductList(ProductList productList) 
        {
            using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
            {
                var productListToRemove = db.ProductLists.SingleOrDefault(a =>
                a.IdProduct == productList.IdProduct 
                && a.IdSale == productList.IdSale);
                if (productListToRemove != null)
                {
                    db.ProductLists.Remove(productListToRemove);
                    db.SaveChanges();
                }
            }
        }
    }
}
