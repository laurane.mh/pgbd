﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyPopUpStore_LM.DAL.DB;

namespace MyPopUpStore_LM.BU
{
    class PriceCategoryService
    {
        //public static Price GetPriceCategoryById(int priceId)
        //{
        //    using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
        //    {
        //        return db.Prices.Find(priceId);
        //    }
        //}

        //public static List<Price> GetPricesCategories()
        //{
        //    using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
        //    {
        //        return db.Prices.ToList();
        //    }
        //}

        public static List<Price> GetPricesCategoriesOfStore(int storeId)
        {
            using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
            {
                return db.Prices.Where(p => p.IdStore == storeId).ToList();
            }
        }

        //public static Price SearchPriceCategoryById (int priceId)
        //{
        //    using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
        //    {
        //        return db.Prices.SingleOrDefault(p => p.IdPrice == priceId);
        //    }
        //}

        public static void AddPriceCategory(Price price)
        {
            using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
            {
                db.Prices.Add(price);
                db.SaveChanges();
            }
        }

        public static void EditPriceCategory(Price price )
        {
            using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
            {
                Price priceToUpdate = db.Prices.Find(price.IdPrice);
                {
                    if (priceToUpdate != null)
                    {
                        priceToUpdate.Amount = price.Amount;
                        priceToUpdate.Color = price.Color;

                        db.Update(priceToUpdate);
                    }
                }
                db.SaveChanges();
            }
        }

        public static void DeletePriceCategory(int priceId)
        {
            using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
            {
                var priceCategoryToRemove = db.Prices.SingleOrDefault(a => a.IdPrice == priceId);
                if (priceCategoryToRemove != null)
                {
                    db.Prices.Remove(priceCategoryToRemove);
                    db.SaveChanges();
                }
            }
        }

        public static bool ProductHasPriceCategory(int priceId)
        {
            using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
            {
                return db.Products.Where(s => s.IdPrice == priceId).Count() > 0;
            }
        }
    }
}
