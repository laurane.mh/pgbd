﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MyPopUpStore_LM.DAL.DB;

namespace MyPopUpStore_LM.BU
{
    class ProductService
    {
        //public static Product GetProductById(int ProductId)
        //{
        //    using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
        //    {
        //        return db.Products.Find(ProductId);
        //    }
        //}

        //public static ObservableCollection<string> GetProductName()
        //{
        //    using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
        //    {
        //        List<string> names = new List<string>();

        //        foreach (Product product in db.Products)
        //        {
        //            names.Add(product.Wording);
        //        }

        //        ObservableCollection<string> myCollection = new ObservableCollection<string>(names);
        //        return myCollection;
        //    }

        //}

        public static List<Product> GetProducts()
        {
            using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
            {
                return db.Products.Include(p => p.IdPriceNavigation).ToList();
            }
        }

        public static List<Product> GetProductsOfStore(int storeId)
        {
            using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
            {
                return db.Products.Where(p => p.IdStore == storeId).Include(p => p.IdPriceNavigation).ToList();
            }
        }

        public static int GetIdProductByCode(string codeProduct)
        {
            using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
            {
                //Product product = (Product)db.Products.Where(p => p.ProductCode == codeProduct);
                //return product.IdProduct;

                return db.Products.Where(p => p.ProductCode == codeProduct).ToList()[0].IdProduct;
            }
        }

        public static void AddProduct(Product product)
        {
            using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
            {
                db.Products.Add(product);
                db.SaveChanges();
            }
        }

        public static void EditProduct(Product product)
        {
            using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
            {
                Product productToUpdate = db.Products.Find(product.IdProduct);
                {
                    if (productToUpdate != null)
                    {
                        productToUpdate.ProductCode = product.ProductCode;
                        productToUpdate.Wording = product.Wording;
                        productToUpdate.IdPrice = product.IdPrice;
                        productToUpdate.Stock = product.Stock;
                        db.Update(productToUpdate);
                    }
                }
                db.SaveChanges();
            }
        }

        public static void DeleteProduct(int productId)
        {
            using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
            {
                var productToRemove = db.Products.SingleOrDefault(a => a.IdProduct == productId);
                if (productToRemove != null)
                {
                    db.Products.Remove(productToRemove);
                    db.SaveChanges();
                }
            }
        }

        public static bool SaleHasProduct(int productId)
        {
            using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
            {
                return db.ProductLists
                    .Include(p => p.IdSaleNavigation)
                    .Include(p => p.IdProductNavigation)
                    .Where(s => s.IdProduct == productId).Count() > 0;
            }
        }

    }
}
