﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyPopUpStore_LM.DAL.DB;
using Microsoft.EntityFrameworkCore;

namespace MyPopUpStore_LM.BU
{
    class SaleService
    {
        public static Sale GetSaleById(int SaleId)
        {
            using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
            {
                return db.Sales.Find(SaleId);
            }
        }

        public static List<Sale> GetSales()
        {
            using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
            {
                return db.Sales.ToList();
            }
        }

        public static int AddSales(Sale sale)
        {
            using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
            {
                db.Sales.Add(sale);
                db.SaveChanges();
                return sale.IdSale;
            }
        }

       
        public static void DeleteSale(int saleId)
        {
            var saleToRemove = GetSaleById(saleId);
            if (saleToRemove != null)
            {
                List<ProductList> productLists = ProductListService.GetProductListsOfSale(saleId);
                foreach(ProductList productList in productLists)
                {
                    ProductListService.DeleteProductList(productList);
                }
                    
                using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
                {
                    db.Sales.Remove(saleToRemove);
                    db.SaveChanges();
                }
            }
        }

        public static void EditSale(Sale sale) 
        {
            using (MyPopUpStoreLauraneContext db = new MyPopUpStoreLauraneContext())
            {
                Sale saleToUpdate = db.Sales.Find(sale.IdSale);
                {
                    if (saleToUpdate != null)
                    {
                        saleToUpdate.DateTime = sale.DateTime;
                        saleToUpdate.PaymentMeans = sale.PaymentMeans;
                        saleToUpdate.ProductLists = sale.ProductLists;
                        db.Update(saleToUpdate);
                    }
                }
                db.SaveChanges();
            }
        }
    }
}

          